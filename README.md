A [joe-alm](https://gitlab.com/Schniz/joe-alm) cli tool 

## Installation
`npm install -g joe-alm`

## Usage
From the `joe-alm --help`:

    Usage: index.js [options]

    Options:

      -h, --help                                  output usage information
      -V, --version                               output the version number
      backlog                                     the backlog
      review                                      the review
      done                                        the done
      todo                                        the todo
      wip                                         see work in progress issues
      -h, --host <https://joe-alm.herokuapp.com>  Joe ALM host
      -p, --project <defaults to origin>          project to query
      -t, --token <access token>                  token
      -a, --all                                   see data from all milestones
      -m, --milestone [milestone]                 specific milestone. defaults to the closest.
      -nc, --no-colors                            remove coloring
