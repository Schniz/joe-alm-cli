import { crash } from './print';
import { backlogQuery } from './queries';
import callGraphql from './callGraphql';

export default function readBacklog(host, token, project) {
  return callGraphql(host, backlogQuery, { path: project }, token).then(results => {
    const projectName = results.data.project.name;
    const issues = results.data.project.backlog;
    return { projectName, issues, type: 'backlog' };
  }).catch(crash);
}
