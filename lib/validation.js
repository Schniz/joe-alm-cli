import { compose } from 'ramda';
import print from './print';

export function checkForToken(prog) {
  if (!process.env.GITLAB_ACCESS_TOKEN && !prog.token) {
    print('please provide a token');
    process.exit(1);
  }

  return prog;
}

export function checkForProject(prog) {
  if (!prog.project) {
    const args = process.argv.slice(2).join(' ');
    print('Project was not supplied and there is no');
    print('`origin\' remote in the current directory');
    print();
    print('You can try launching with:');
    print(`joe-alm ${args} -p namespace/some-project`);
    process.exit(1);
  }

  return prog;
}

export default compose(checkForProject, checkForToken);
