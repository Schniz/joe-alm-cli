import program from 'commander-plus';
import pkg from '../package.json';
import getGitProject from './getGitProject';
import prettyprint from './prettyprint';
import exec from './exec';

const gitProject = getGitProject();
const accessToken = process.env.GITLAB_ACCESS_TOKEN;
const joeAlmHost = process.env.JOE_ALM_HOST || 'https://joe-alm.herokuapp.com';

program
.version(pkg.version)
  .option('backlog', 'the backlog')
  .option('review', 'the review')
  .option('done', 'the done')
  .option('todo', 'the todo')
  .option('wip', 'see work in progress issues')
  .option(`-h, --host <${joeAlmHost}>`, 'Joe ALM host', joeAlmHost)
  .option(`-p, --project <${gitProject || 'gitlab project'}>`, 'project to query', gitProject)
  .option(`-t, --token <${accessToken || 'access token'}>`, 'token', accessToken)
  .option('-a, --all', 'see data from all milestones')
  .option('-m, --milestone [milestone]', 'specific milestone. defaults to the closest.')
  .option('-nc, --no-colors', 'remove coloring')
  .parse(process.argv);

exec(program).then(prettyprint);
