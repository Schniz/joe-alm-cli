import capitalize from 'capitalize';
import print, { crash } from './print';

function extractAssignee({ assignee }) {
  if (!assignee) {
    return '';
  }

  return ` @${assignee.username} (${assignee.name})`.grey;
}

function extractId({ iid }) {
  return `#${iid}`.red;
}

export default function prettyprint({ projectName, issues, type }) {
  const title = `${capitalize(type)} issues in ${projectName}`;
  print(title.cyan.bold);
  print(Array(title.length + 1).join('-').cyan);

  if (issues.length) {
    issues.map(issue => `- ${extractId(issue)} ${issue.title}${extractAssignee(issue)}`).forEach(e => print(e));
  } else {
    crash('No issues satisfy this filter.');
  }
}
