import fetch from 'isomorphic-fetch';
import createDebugger from './debug';

const logger = createDebugger('callGraphql');
const { debug } = logger('response');

export default function callGraphql(host, query, variables, token) {
  const url = `${host}/graphql`;
  logger.debug(`fetching ${url}`);
  return fetch(url, {
    method: 'POST',
    headers: {
      token,
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({
      query,
      variables: JSON.stringify(variables)
    })
  }).then(e => e.json()).then(debug);
}


