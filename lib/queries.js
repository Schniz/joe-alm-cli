import { readFileSync } from 'fs';
import { resolve } from 'path';
import { compose, toString } from 'ramda';

const read = compose(toString, readFileSync, resolve.bind(null, __dirname, '..', 'queries'));

export const backlogQuery = read('backlogQuery.graphql');
export const readQuery = read('readQuery.graphql');
