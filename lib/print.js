import { strip } from 'colors';

let toColor = true;

export function setColoring(coloring) {
  toColor = Boolean(coloring);
}

export default function print(...args) {
  let newArgs = toColor ? args : args.map(strip);
  console.log(...newArgs); // eslint-disable-line
}

export function crash(...args) {
  print('error'.red, ...args);
  process.exit(1);
}
