import { execSync } from 'child_process';

function getUrl() {
  try {
    return execSync('git config --get remote.origin.url').toString();
  } catch (e) {
    return undefined;
  }
}

export default function getProject() {
  const regex = /(?:https?:\/\/.*\/|git@.*:)(.+)\/(.+)\.git/;
  const match = regex.exec(getUrl());
  const namespace = match[1];
  const project = match[2];
  return `${namespace}/${project}`;
}
