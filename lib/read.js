import { crash } from './print';
import { flatten, map, compose } from 'ramda';
import { readQuery } from './queries';
import callGraphql from './callGraphql';

const getIssues = () => compose(flatten, map(milestone => milestone.issuesForType));

export default function read({
  host,
  type,
  program: {
    token,
    project,
    //  milestone,
    all
  }
}) {
  if (all) {
    return callGraphql(host, readQuery, {
      path: project,
      type
    }, token).then(results => {
      const projectName = results.data.project.name;
      const issues = getIssues(type)(results.data.project.milestones);
      return { projectName, type, issues };
    }).catch(crash);
  }

  return crash('to do...');
}
