import { compose } from 'ramda';
import validate from './validation';
import readBacklog from './readBacklog';
import read from './read';
import print, { crash, setColoring } from './print';

function setColor(program) {
  setColoring(program.colors);
  return program;
}

export function query(program) {
  if (program.backlog) {
    return readBacklog(program.host, program.token, program.project);
  }

  let matched = ['wip', 'done', 'todo', 'review'].find(key => program[key]);

  if (!matched) {
    print('You need to choose SOMETHING.');
    print();
    print('you can see the help with:');
    print('joe-alm --help');
    return crash('please try again');
  }

  return read({ type: matched, program: program, host: program.host });
}

export default compose(query, validate, setColor);
